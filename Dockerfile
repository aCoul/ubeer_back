# Dockerfile (ubeer_back/Dockerfile)

# Utilisation de l'image Maven 3.8.5 avec OpenJDK 17 comme base
FROM maven:3.8.5-openjdk-17 AS build

# Définition du répertoire de travail dans l'image
WORKDIR /app

# Copie du fichier de configuration Maven (pom.xml) dans le répertoire de travail
COPY pom.xml .

# Copie du répertoire source de l'application dans le répertoire de travail
COPY src ./src

# Exécution de la commande Maven pour construire l'application. "-DskipTests" permet de sauter l'exécution des tests pendant la construction.
RUN mvn clean package -DskipTests

# Deuxième étape: Utilisation de l'image OpenJDK 17 (version slim) comme base pour exécuter l'application
FROM openjdk:17-jdk-slim

# Définition du répertoire de travail dans l'image
WORKDIR /app

# Copie du fichier JAR de l'étape précédente (étape build) dans le répertoire de travail de cette étape
COPY --from=build /app/target/*.jar app.jar

# Exposition du port 9002 pour que l'application puisse recevoir du trafic externe
EXPOSE 9002

# Commande d'entrée qui sera exécutée lorsque le conteneur sera démarré
ENTRYPOINT ["java", "-jar", "app.jar"]
